/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enum;

/**
 *
 * @author WPOSS
 */
public enum InitParameters {
    INIT_FIELD,
    BITMAP,
    PAN,
    PROCCESING_CODE,
    AMOUNT,
    FIELD5,
    FIELD6,
    DATETIME,
    FIELD8,
    FIELD30,
    FIELD31,
    FIELD32,
    FIELD33,
    FIELD34,
    TRACK2,
    FIELD36,
    RRN,
    AUTH_ID,
    RESPONSE_CODE,
    FIELD40,
    TERMINAL_ID,
    MERCHANT_ID,
    FIELD43,
    FIELD44,
    TRACK1,
    FIELD46,
    FIELD47,
    FIELD48,
    FIELD49,
    FIELD50,
    FIELD51,
    PINBLOCK,
    FIELD53,
    ADDITIONAL_AMOUNTS,
    FIELD55,
    FIELD56,
    FIELD57,
    FIELD58,
    FIELD59,
    FIELD60,
    FIELD61,
    FIELD62,
    FIELD63,
    FIELD64,
    END_FIELD,
}
