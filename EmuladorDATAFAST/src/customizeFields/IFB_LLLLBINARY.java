/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customizeFields;

import org.jpos.iso.BinaryPrefixer;
import org.jpos.iso.ISOBinaryFieldPackager;
import org.jpos.iso.LiteralBinaryInterpreter;

/**
 *
 * @author WPOSS
 */
public class IFB_LLLLBINARY extends ISOBinaryFieldPackager {

    public IFB_LLLLBINARY() {
        super(LiteralBinaryInterpreter.INSTANCE, BinaryPrefixer.BB);
    }

    /**
     * @param len - field len
     * @param description symbolic descrption
     */
    public IFB_LLLLBINARY(int len, String description) {
        super(len, description, LiteralBinaryInterpreter.INSTANCE, BinaryPrefixer.BB);
        checkLength(len, 65000);
    }

    public void setLength(int len) {
        checkLength(len, 65000);
        super.setLength(len);
    }
}
