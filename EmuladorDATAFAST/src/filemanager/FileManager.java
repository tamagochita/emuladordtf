/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemanager;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

/**
 *
 * @author WPOSS
 */
public class FileManager {

    public static File file;
    public static String path = "C:\\Users\\WPOSS\\Documents\\NetBeansProjects\\EmuladorDATAFAST";

    public long calculateOffset(String pathDefault, String fileName, boolean deleteFile) {
        long len = 0;
        File dir = new File(pathDefault);
        file = new File(pathDefault + File.separator + fileName);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        if (file.exists()) {
            if (file.delete()) {
                System.out.println("archivo eliminado");
            }

            len = file.length();

        } else {
            try {
                if (!file.createNewFile()) {
                    System.out.println("create file fail");
                    return -1;
                }
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return len;
    }

}
