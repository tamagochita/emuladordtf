/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fixedData;

/**
 *
 * @author WPOSS
 */
public class FixedData {
     public static final String VERSION = "v1.0";
    public static final String CONFIG_FILE = "./fieldsIsoDATAFAST.properties";

    //Comm
    public static final String IP = "IP";
    public static final String PORT = "PORT";
    public static final String TIMEOUT = "TIMEOUT";
    public static final String TPDU = "TPDU";
    //Fields
    public static final String INIT_FIELD = "MTI";
    public static final String PROCCESING_CODE = "PROCCESING_CODE";
    public static final String AMOUNT = "AMOUNT";
    public static final String STAN = "TRACE_NUMBER";
    public static final String TERMINAL_ID = "TERMINAL_ID";
    public static final String MERCHANT_ID = "MERCHARD_ID";
    public static final String FIELD60 = "FIELD60";

    //WS
    public static final String REQ_JSON1 = "REQ_JSON1";
    public static final String REQ_JSON2 = "REQ_JSON2";
}
