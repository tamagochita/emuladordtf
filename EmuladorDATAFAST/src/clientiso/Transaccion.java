/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientiso;

import customizeFields.ISO87BPackager_TeleLoader;
import filemanager.FileManager;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import static main.Main.seq;
import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.channel.NACChannel;
import org.jpos.util.LogSource;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;
import utils.IsoUtils;

/**
 *
 * @author WPOSS
 */
public class Transaccion {

    long offset, length = 3000;

    boolean flagDload = true;
    String Fld64 = "";
    String Fld63 = "";
    int TmpLenFld64 = 0;
    FileOutputStream fileOutputStream;
    FileOutputStream fileOutputStreamKey;
    FileManager filemanager = new FileManager();
    FileManager filemanagerKey = new FileManager();
    byte[] f64;
    byte[] f63;
    boolean flag = true;
    IsoUtils Utils = new IsoUtils();
    private final int id;
    IsoUtils utils = new IsoUtils();
    String cliente = "DATAFAST";
    String tipo = "POS";
    String marca = "NEWPOS";
    String Modelo = "NEW9220";
    String interfac = "ETHERNET";
    String ip = "186.154.93.81";
    String mac = "a4:86:ae:63:44:58";
    String imei = "865067037733949";
    String serial = "9220013414";
    String serialSim = "8959102072049203966F";
    String latitud = "0";
    String longitud = "0";
    String apn = "NA";
    String nivelSeñal = "100";
    String nivelBateria = "39";
    String infCel = "736,2,44303,7689476";
    String reser1 = "NA";
    String reser2 = "NA";
    String ver = "Polaris Cloud_6_18";
    String androudid = "5b7988f09a13b9b4";
    String nomapp = "pruebaGochi\\9220_datafast_prod_7.3.4.apk";
    int puerto = 8013;

    public Transaccion(int idx) {
        this.id = idx;
    }

    public void run() {
        try {
            ///Crear Logger
            Logger logger = new Logger();
            logger.addListener(new SimpleLogListener(System.out));
            ///Crear Channel
            byte[] TPDU = {0x60, 0x00, 0x01, 0x00, 0x00};
            //  ISOChannel channel = new NACChannel("186.154.93.81", 5555, new ISO87BPackager_TeleLoader(), TPDU);
            ISOChannel channel = new NACChannel(ip, puerto, new ISO87BPackager_TeleLoader(), TPDU);

            ///Establecer Logger al canal
            ((LogSource) channel).setLogger(logger, "ClientChannel");
            ((BaseChannel) channel).setTimeout(90000);   ///30 segundos de espera

            ///Conectar al Servidor 
            channel.connect();
            offset = filemanager.calculateOffset(filemanager.path, "file.apk", true);
            transInitDATAFAST(channel);
            channel.disconnect();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void transInitDATAFAST(ISOChannel channel) throws ISOException, IOException, InterruptedException {
        boolean flagDload = true;
        final String TID = "16024867";

        String fld60Rcv = "";
        String Fld64 = "";
        FileOutputStream fileOutputStream;
        FileManager filemanager = new FileManager();
        byte[] f64;
        long offset = 0, length = 9000;
        offset = filemanager.calculateOffset(filemanager.path, TID + ".zip", true);
        String prodCode;
        prodCode = "930020";
        while (flagDload) {
            ISOMsg m = new ISOMsg();
            m.setMTI("0800");
            m.set(3, prodCode);
            m.set(new ISOField(11, ISOUtil.zeropad(new Integer(seq.get("traceno")).toString(), 6)));
            m.set(41, TID);
            m.set(61, cliente + "|" + tipo + "|" + marca + "|" + Modelo + "|" + interfac + "|" + ip + "|" + mac + "|" + imei + "|" + serial + "|" + serialSim + "|" + latitud + "|" + longitud + "|" + apn + "|" + nivelSeñal + "|" + nivelBateria + "|" + infCel + "|" + reser1 + "|" + reser2 + "|" + ver + "|" + "|" + androudid);
            m.set(64, nomapp.getBytes());
            ///Enviar Mensaje a través del canal creado
            channel.send(m);
            ISOMsg r = channel.receive();
            if ((r.getString(39).equals("00"))) {

                if ((r.getString(41).equals(TID))) {
                    prodCode = r.getString(3);
                    fld60Rcv = r.getString(60);
                    if (r.getString(60).equals("ACTUALIZACION APROBADA CONFIGURACION X TERMINAL")) {
                        descarga();
                        break;
                    }
                    System.out.println("TRANSACCION APROBADA: ");
                   
               

                } else {
                    System.out.println("TRANSACCION RECHAZADA: TID recibido no "
                            + "coincide con el del requerimiento");
                    flagDload = false;  /// Break download process
                }

                if (r.getString(3).equals("930020")) {
                    prodCode = "930020";
                    flagDload = false;
                }
            } else {
                System.out.println("TRANSACCION RECHAZADA: " + r.getString(60));
                flagDload = false;  /// Break download process
            }
        }
    }

    private void descarga() throws ISOException, IOException, InterruptedException {

        try {
            ///Crear Logger
            Logger logger = new Logger();
            logger.addListener(new SimpleLogListener(System.out));

            Date date = new Date();
            SimpleDateFormat field12Format = new SimpleDateFormat("HHmmss");
            SimpleDateFormat field13Format = new SimpleDateFormat("MMddYYYY");

            seq.set("traceno", 1);

            ///Crear Channel
            byte[] TPDU = {0x60, 0x00, 0x01, 0x00, 0x00};
            ISOChannel channel = new NACChannel("186.154.93.81",8013 , new ISO87BPackager_TeleLoader(), TPDU);

            ///Establecer Logger al canal
            ((LogSource) channel).setLogger(logger, "ClientChannel");
            ((BaseChannel) channel).setTimeout(90000);   ///30 segundos de espera

            ///Conectar al Servidor 
            channel.connect();
            offset = filemanager.calculateOffset(filemanager.path, "file.apk", true);

            while (flagDload) {
                ///Crear mensaje ISO y campos del mismo
                ISOMsg m = new ISOMsg();

                m.setMTI("0800");     ///
                m.set(3, "930080");    ///
                m.set(new ISOField(11, ISOUtil.zeropad(Integer.toString(seq.get("traceno")), 6)));
                m.set(12, field12Format.format(date));
                m.set(13, field13Format.format(date));
                m.set(24, "021");
                m.set(41, "00000006");  ///Terminal ID 
                m.set(60, nomapp + "," + offset + "," + length);// Modelo del terminal,offset,length
                m.set(61, cliente + "|" + tipo + "|" + marca + "|" + Modelo + "|" + interfac + "|" + ip + "|" + mac + "|" + imei + "|" + serial + "|" + serialSim + "|" + latitud + "|" + longitud + "|" + apn + "|" + nivelSeñal + "|" + nivelBateria + "|" + infCel + "|" + reser1 + "|" + reser2 + "|" + ver + "|" + "|" + androudid);//Primer NA sera el hash de certificado  y segundo promociones
                ///Enviar Mensaje a través del canal creado
                channel.send(m);

                ISOMsg r = channel.receive();

                if (r.getString(39) != null) {
                    if (r.getString(39).equals("00")) {

                        System.out.println("TRANSACCION APROBADA: ");

                        if (r.getString(64) != null) {
                            Fld64 = r.getString(64);
                            f64 = Utils.hexStringToByteArray(Fld64);

                            fileOutputStream = new FileOutputStream(FileManager.file, true);
                            fileOutputStream.write(f64, 0, f64.length);
                            System.out.println("offset: " + offset);
                            System.out.println("f64.length: " + f64.length);
                            offset += f64.length;
                            byte[] dat = ISOUtil.hex2byte(r.getString(64));
                            String str = new String(dat, StandardCharsets.UTF_8);
                            System.out.println("Campo 64 Contenido del archivo:\n " + str);
                            if (r.getString(60) == "DESCARGA DE ARCHIVO OK (FINALIZADO) PORCENTAJE: 100") {
                                flagDload = false;
                                channel.disconnect();
                            }

                        } else {
                            flagDload = false; /// Break download process
                            break;
                        }
                    } else {
                        System.out.println("TRANSACCION RECHAZADA: " + r.getString(60));
                        flagDload = false; /// Break download process
                    }
                } else {
                    System.out.println("TRANSACCION RECHAZADA: " + r.getString(60));
                    flagDload = false; /// Break download process
                }
                if (r.getString(3).equals("950000")) {
                    flagDload = false; /// Break download process
                    System.out.println("BD: " + Fld64);
                } else {
                    continue;
                }
                Thread.sleep(10);

            }//fin while
        } catch (IOException | ISOException e) {
            System.out.println(e);
        }
    }

}
